// ----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __JCLIENT_H
#define __JCLIENT_H


#include <inttypes.h>
#include <stdlib.h>
#include <math.h>
#include <clthreads.h>
#include <jack/jack.h>
#include "dplimit1.h"
#include "global.h"


class Jclient : public A_thread
{
public:

    Jclient (const char *jname, const char *jserv, int nchan);
    ~Jclient (void);

    const char *jname (void) const { return _jname; }

    void set_inpgain (float v)
    {
	_ipg0 = powf (10.0f, 0.05f * v);
    }

    void set_threshd (float v) 
    {
	_dplimit.set_threshd (v);
    }

    void set_reltime (float v)
    {
	_dplimit.set_reltime (v);
    }

    Dplimit1 *dplimit (void) const { return (Dplimit1 *) &_dplimit; }

private:

    void  init_jack (const char *jname, const char *jserv);
    void  close_jack (void);
    void  jack_shutdown (void);
    int   jack_process (int nframes);

    virtual void thr_main (void) {}

    jack_client_t  *_jack_client;
    jack_port_t    *_inpports [MAXCHAN];
    jack_port_t    *_outports [MAXCHAN];
    bool            _active;
    const char     *_jname;
    unsigned int    _fsamp;
    int             _nchan;
    int             _fragm;
    int             _nsamp;
    float           _ipg0;
    float           _ipg1;
    float           _dipg;
    Dplimit1        _dplimit;

    static void jack_static_shutdown (void *arg);
    static int  jack_static_process (jack_nframes_t nframes, void *arg);
};


#endif
