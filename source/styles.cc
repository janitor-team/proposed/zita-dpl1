// ----------------------------------------------------------------------------
//
//  Copyright (C) 2008-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include "styles.h"
#include "png2img.h"


XftColor      *XftColors [NXFTCOLORS];
XftFont       *XftFonts [NXFTFONTS];

XImage     *parsect_img;
XImage     *redzita_img;
XImage     *hmeter0_img;
XImage     *hmeter1_img;
RotaryGeom  r_ipgain_geom;
RotaryGeom  r_thresh_geom;
RotaryGeom  r_reltim_geom;


void styles_init (X_display *disp, X_resman *xrm)
{
    XftColors [C_MAIN_BG] = disp->alloc_xftcolor (0.25f, 0.25f, 0.25f, 1.0f);
    XftColors [C_MAIN_FG] = disp->alloc_xftcolor (1.0f, 1.0f, 1.0f, 1.0f);
    XftColors [C_DISP_BG] = disp->alloc_xftcolor (0.1f, 0.1f, 0.1f, 1.0f);
    XftColors [C_TEXT_BG] = disp->alloc_xftcolor (0.9f, 0.9f, 0.9f, 1.0f);
    XftColors [C_TEXT_FG] = disp->alloc_xftcolor (0.0f, 0.0f, 0.0f, 1.0f);
    XftColors [C_INPGAIN] = disp->alloc_xftcolor (0.9f, 0.9f, 0.2f, 1.0f);
    XftColors [C_THRESHD] = disp->alloc_xftcolor (1.0f, 0.6f, 0.2f, 1.0f);
    XftColors [C_RELTIME] = disp->alloc_xftcolor (0.6f, 0.6f, 1.0f, 1.0f);
 
    XftFonts [F_PARAMS] = disp->alloc_xftfont (xrm->get (".font.params", "luxi:bold::pixelsize=11"));

    parsect_img = png2img (SHARED"/parsect.png", disp, XftColors [C_MAIN_BG]);
    redzita_img = png2img (SHARED"/redzita.png", disp, XftColors [C_MAIN_BG]); 
    hmeter0_img = png2img (SHARED"/hmeter0.png", disp, XftColors [C_DISP_BG]);
    hmeter1_img = png2img (SHARED"/hmeter1.png", disp, XftColors [C_DISP_BG]);

    if (!parsect_img || !redzita_img || !hmeter0_img || !hmeter1_img)
    {
	fprintf (stderr, "Can't load images from '%s'.\n", SHARED);
	exit (1);
    }

    r_ipgain_geom._backg = XftColors [C_MAIN_BG];
    r_ipgain_geom._image [0] = parsect_img;
    r_ipgain_geom._lncol [0] = 0;
    r_ipgain_geom._x0 = 30;
    r_ipgain_geom._y0 = 17;
    r_ipgain_geom._dx = 23;
    r_ipgain_geom._dy = 23;
    r_ipgain_geom._xref = 11.5;
    r_ipgain_geom._yref = 11.5;
    r_ipgain_geom._rad = 11;

    r_thresh_geom._backg = XftColors [C_MAIN_BG];
    r_thresh_geom._image [0] = parsect_img;
    r_thresh_geom._lncol [0] = 0;
    r_thresh_geom._x0 = 105;
    r_thresh_geom._y0 = 17;
    r_thresh_geom._dx = 23;
    r_thresh_geom._dy = 23;
    r_thresh_geom._xref = 11.5;
    r_thresh_geom._yref = 11.5;
    r_thresh_geom._rad = 11;

    r_reltim_geom._backg = XftColors [C_MAIN_BG];
    r_reltim_geom._image [0] = parsect_img;
    r_reltim_geom._lncol [0] = 0;
    r_reltim_geom._x0 = 180;
    r_reltim_geom._y0 = 17;
    r_reltim_geom._dx = 23;
    r_reltim_geom._dy = 23;
    r_reltim_geom._xref = 11.5;
    r_reltim_geom._yref = 11.5;
    r_reltim_geom._rad = 11;
}


void styles_fini (X_display *disp)
{
    XDestroyImage (parsect_img);
    XDestroyImage (redzita_img);
    XDestroyImage (hmeter0_img);
    XDestroyImage (hmeter1_img);
}
